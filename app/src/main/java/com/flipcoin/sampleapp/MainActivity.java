package com.flipcoin.sampleapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.flipcoins.sdk.FlipcoinsInit;

public class MainActivity extends AppCompatActivity {

    private FlipcoinsInit builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        builder = new FlipcoinsInit.FlipcoinsInitBuilder(MainActivity.this)
                .build();
    }

    @Override
    protected void onResume() {
        super.onResume();
        builder.init();
    }

    @Override
    protected void onPause() {
        super.onPause();
        builder.stopHandler();
    }
}
