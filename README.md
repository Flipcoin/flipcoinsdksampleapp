# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

### Summary ###
Flipcoin Sdk Integration with your app.

**Version 1.0**


### How do I get set up? ###


Download from(choose any one):

### Dependency :

### MAVEN ###

    <dependency>

       <groupId>com.flipcoins.sdk</groupId>

      <artifactId>flipcoins</artifactId>

      <version>1.0.0</version>

      <type>pom</type> 

    </dependency> 

### Gradle ###

    compile 'com.flipcoins.sdk:flipcoins:1.0.0'

### Q. How to integrate sdk with your app(Using Gradle)? ###

### Note: Your application must have Internet Permission. ###

### 
###**Step 1:**

Add following code in you project gradle file:  


    repositories {

      maven {

            url 'https://flipcoin-mobi.bintray.com/FlipcoinAndroidSdk'

           }

    }

// add dependency in dependencies section 

      compile 'com.flipcoins.sdk:flipcoins:1.0.0'







### **Step 2:**

In your MainActivity of Your project you need to do:

A) Create FlipcoinsInitBuilder object in onCreate() method


B) call method init() - Initiate sdk in onResume() 


C) call method stopHandler() - Stop sdk in onPause()







### ** A) Proguard Rules:**

-keep com.flipcoins.sdk.**  { * ; }

-dontwarn com.flipcoins.**




###
### **Who do I talk to?** 

 **Contact Us :** [support@flipcoin.mobi](Link URL)